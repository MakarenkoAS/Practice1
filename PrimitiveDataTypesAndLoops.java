package ua.org.oa.makarenkoas.Practice1;

import java.util.Iterator;

//����� ��� ������� �1
public class PrimitiveDataTypesAndLoops {
	public static byte fieldB;
	public static short fieldS;
	public static int fieldI;
	public static long fieldL;
	public static char fieldC;
	public static float fieldF;
	public static double fieldD;

	public static byte printPrimitiveDataTypes(byte c) {
		return c;
	}

	// ����� ��� ������� �4
	public static String chekTriangle(int a4, int b4, int c4) {
		int sqrCthetus = a4 * a4 + b4 * b4;
		int sqrHypotenuse = c4 * c4;
		String test = (sqrHypotenuse == sqrCthetus) ? "Triangle is rectangular" : "Triangle is common";
		return test;
	}

	// ����� ��� ������� �5
	public static int calculateSum(int a5, int b5) {
		int sum = 0;
		for (int i = 0; i < 21; i++) {
			sum += i;
		}
		return sum;
	}

	// ����� ��� ������� �6
	public static int calculateSumOfEven(int a6, int b6) {
		int sumOfIntegers = 0;
		for (int i = 0; i < 21; i = i + 2) {
			sumOfIntegers += i;
		}
		return sumOfIntegers;
	}

	// ����� ��� ������� �7
	public static int calculateSumOfSimpleNums(int a7, int b7) {
		int sumOfSimpleNums = 2;
		for (int i = 2; i < 21; i = i + 1) {
			if (i <= 7 && i % 2 != 0) { 										 //����������� ����������� ������� �����
				sumOfSimpleNums += i;
			}
			if (i > 7 && i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0) { //����������� ��������� ������� �����
				sumOfSimpleNums += i;
			}

		}
		return sumOfSimpleNums;
	}
	//����� ��� ������� �8
	public static int calculateSumOfTwo(int a8, int b8, int c8) {
		int sumABC = 0;
		while ((a8 + b8) - c8 == sumABC||(b8+c8)-a8==sumABC||(c8+a8)-b8==sumABC) {
			System.out.println("True");

			break;
		}
		return sumABC;
		
	}
	//����� ��� ������� �9
	public static double calculateAverageOfRow(int a9, int b9) {
		double a9B9 = 0;
		if (a9 >= 0 && b9 >= 0 && a9 > b9) {	//������� ������
			for (int i = a9; i >= b9; i--) {
				a9B9 += i;						//������� ����� ����
			}
			a9B9 = a9B9 / (a9 - b9+1);			//���������� �������� ��������
		}
		return a9B9;
	}
	//����� ������� �10 
	public static double calculateCreditPaymentPerMonth(double sumOfCerdit, double interestRate, int creditPeriod) {
		double creditPayment = 0;
		double totalCreditPayment = 0;
		double paymentPerMonth = sumOfCerdit / creditPeriod;
		for (int i = 1; i <= creditPeriod; i++) { // ���� ��� ����� ������� �
													// ������ �� ������� � �����
			creditPayment = (paymentPerMonth + (sumOfCerdit - (paymentPerMonth * (i - 1))) * (interestRate / 12));
			totalCreditPayment = totalCreditPayment + creditPayment; // ������ ������ �����, 
																	 // ����������� �� �������
			System.out.println("The credit payment in " + i + " month is " + creditPayment + " $");
		}
		System.out.println("Total credit rates payment " + totalCreditPayment + " $");

		return creditPayment;

	}

}
