package ua.org.oa.makarenkoas.Practice1;

public class PrimitiveDataTypesAndLoopsTest {

	public static void main(String[] args) {
		System.out.println("~~~Task 1~~~~"); // ������� �1
		System.out.println("Default value of byte field is " + PrimitiveDataTypesAndLoops.fieldB);
		System.out.println("Default value of short field is " + PrimitiveDataTypesAndLoops.fieldS);
		System.out.println("Default value of integer field is " + PrimitiveDataTypesAndLoops.fieldI);
		System.out.println("Default value of long field is " + PrimitiveDataTypesAndLoops.fieldL);
		System.out.println("Default value of char field is " + PrimitiveDataTypesAndLoops.fieldC);
		System.out.println("Default value of float field is " + PrimitiveDataTypesAndLoops.fieldF);
		System.out.println("Default value of double field is " + PrimitiveDataTypesAndLoops.fieldF);
		System.out.println(" ");

		System.out.println("~~~Task 2~~~"); // ������� �2
		float f1 = (float) 1.;
		float f2 = 1;
		float f3 = 0x1;
		float f4 = 0b1;
		float f5 = (float) 0.1e1;
		float f6 = 01;
		System.out.println("Float= " + f1 + " " + f2 + " " + f3 + " " + f4 + " " + f5 + " " + f6);

		System.out.println("~~~Task 4~~~~"); // ������� �4
		int a4 = 3;
		int b4 = 4;
		int c4 = 5;
		String test = PrimitiveDataTypesAndLoops.chekTriangle(a4, b4, c4);
		System.out.println(test);
		System.out.println(" ");

		System.out.println("~~~Task5~~~"); // ������� �5
		int a5 = 0;
		int b5 = 21;
		int sum = PrimitiveDataTypesAndLoops.calculateSum(a5, b5);
		System.out.println("The sum of numbers from 1 to 20 is " + sum);
		System.out.println(" ");

		System.out.println("~~~Task6~~~"); // ������� �6
		int a6 = 0;
		int b6 = 21;
		int sumOfIntegers = PrimitiveDataTypesAndLoops.calculateSumOfEven(a6, b6);
		System.out.println("The sum of even numbers from 1 to 20 is " + sumOfIntegers);
		System.out.println(" ");

		System.out.println("~~~Task7~~~"); // ������� �7
		int a7 = 0;
		int b7 = 21;
		int sumOfSimpleNums = PrimitiveDataTypesAndLoops.calculateSumOfSimpleNums(a7, b7);
		System.out.println("The sum of simple numbers from 1 to 20 is " + sumOfSimpleNums);
		System.out.println(" ");

		System.out.println("~~~Task8~~~"); // ������� �8
		int a8 = 5;
		int b8 = 8;
		int c8 = 3;
		int sumABC = PrimitiveDataTypesAndLoops.calculateSumOfTwo(a8, b8, c8);
		System.out.println(" ");

		System.out.println("~~~Task9~~~"); // ������� �9
		int a9 = 5;
		int b9 = 2;
		double a9B9 = PrimitiveDataTypesAndLoops.calculateAverageOfRow(a9, b9);
		System.out.println("The average of numbers beetween a9 and b9 is " + a9B9);
		System.out.println(" ");

		System.out.println("~~~Task10~~~"); //������� �10
		double sumOfCerdit = 100_000;
		double interestRate = 0.18;
		int creditPeriod = 36;
		double creditPayment = PrimitiveDataTypesAndLoops.calculateCreditPaymentPerMonth(sumOfCerdit, interestRate,
				creditPeriod);
		System.out.println(" ");
	}

}
